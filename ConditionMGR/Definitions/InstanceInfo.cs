﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionMGR.Definitions
{
    enum InstanceInfo
    {
        INSTANCE_INFO_DATA = 0,
        INSTANCE_INFO_GUID_DATA,
        INSTANCE_INFO_BOSS_STATE,
        INSTANCE_INFO_DATA64
    }
}
