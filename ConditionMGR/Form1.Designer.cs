﻿namespace ConditionMGR
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NotesRTB = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ConditionTargetNUD = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.SourceIDNUD = new System.Windows.Forms.NumericUpDown();
            this.SourceIDLBL = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SourceEntryNUD = new System.Windows.Forms.NumericUpDown();
            this.SourceGroupNUD = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SourceTypeOrReferenceIDCMB = new System.Windows.Forms.ComboBox();
            this.SourceGroupTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.SourceEntryTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.SourceIDTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ConditionTargetTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ElseGroupNUD = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.ElseGroupTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ConditionValue3NUD = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.ConditionValue2NUD = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.ConditionValue1NUD = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.NegativeConditionNUD = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.CommentTXT = new System.Windows.Forms.TextBox();
            this.ScriptNameTXT = new System.Windows.Forms.TextBox();
            this.ErrorTypeNUD = new System.Windows.Forms.NumericUpDown();
            this.ErrorTextIDNUD = new System.Windows.Forms.NumericUpDown();
            this.ConditionValue1Tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ConditionValue2Tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ConditionValue3Tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.NegativeConditionTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ErrorTypeTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ErrorTextIdTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ScriptNameTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.GenerateConditionBTN = new System.Windows.Forms.Button();
            this.ResetBTN = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionTargetNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SourceIDNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SourceEntryNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SourceGroupNUD)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ElseGroupNUD)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionValue3NUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionValue2NUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionValue1NUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NegativeConditionNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorTypeNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorTextIDNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NotesRTB);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ConditionTargetNUD);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SourceIDNUD);
            this.groupBox1.Controls.Add(this.SourceIDLBL);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SourceEntryNUD);
            this.groupBox1.Controls.Add(this.SourceGroupNUD);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.SourceTypeOrReferenceIDCMB);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 305);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1.";
            // 
            // NotesRTB
            // 
            this.NotesRTB.Location = new System.Drawing.Point(7, 216);
            this.NotesRTB.Name = "NotesRTB";
            this.NotesRTB.ReadOnly = true;
            this.NotesRTB.Size = new System.Drawing.Size(254, 83);
            this.NotesRTB.TabIndex = 11;
            this.NotesRTB.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Notes";
            // 
            // ConditionTargetNUD
            // 
            this.ConditionTargetNUD.Location = new System.Drawing.Point(158, 157);
            this.ConditionTargetNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ConditionTargetNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ConditionTargetNUD.Name = "ConditionTargetNUD";
            this.ConditionTargetNUD.Size = new System.Drawing.Size(103, 20);
            this.ConditionTargetNUD.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Condition Target";
            // 
            // SourceIDNUD
            // 
            this.SourceIDNUD.Location = new System.Drawing.Point(7, 157);
            this.SourceIDNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.SourceIDNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.SourceIDNUD.Name = "SourceIDNUD";
            this.SourceIDNUD.Size = new System.Drawing.Size(97, 20);
            this.SourceIDNUD.TabIndex = 7;
            // 
            // SourceIDLBL
            // 
            this.SourceIDLBL.AutoSize = true;
            this.SourceIDLBL.Location = new System.Drawing.Point(6, 140);
            this.SourceIDLBL.Name = "SourceIDLBL";
            this.SourceIDLBL.Size = new System.Drawing.Size(52, 13);
            this.SourceIDLBL.TabIndex = 6;
            this.SourceIDLBL.Text = "SourceID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Source Entry";
            // 
            // SourceEntryNUD
            // 
            this.SourceEntryNUD.Location = new System.Drawing.Point(158, 99);
            this.SourceEntryNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.SourceEntryNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.SourceEntryNUD.Name = "SourceEntryNUD";
            this.SourceEntryNUD.Size = new System.Drawing.Size(103, 20);
            this.SourceEntryNUD.TabIndex = 4;
            // 
            // SourceGroupNUD
            // 
            this.SourceGroupNUD.Location = new System.Drawing.Point(6, 99);
            this.SourceGroupNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.SourceGroupNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.SourceGroupNUD.Name = "SourceGroupNUD";
            this.SourceGroupNUD.Size = new System.Drawing.Size(98, 20);
            this.SourceGroupNUD.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Source Group";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "SourceTypeOrReferenceId";
            // 
            // SourceTypeOrReferenceIDCMB
            // 
            this.SourceTypeOrReferenceIDCMB.FormattingEnabled = true;
            this.SourceTypeOrReferenceIDCMB.Items.AddRange(new object[] {
            "NONE",
            "CREATURE_LOOT_TEMPLATE",
            "DISENCHANT_LOOT_TEMPLATE",
            "FISHING_LOOT_TEMPLATE",
            "GAMEOBJECT_LOOT_TEMPLATE",
            "ITEM_LOOT_TEMPLATE",
            "MAIL_LOOT_TEMPLATE",
            "MILLING_LOOT_TEMPLATE",
            "PICKPOCKETING_LOOT_TEMPLATE",
            "PROSPECTING_LOOT_TEMPLATE",
            "REFERENCE_LOOT_TEMPLATE",
            "SKINNING_LOOT_TEMPLATE",
            "SPELL_LOOT_TEMPLATE",
            "SPELL_IMPLICIT_TARGET",
            "GOSSIP_MENU",
            "GOSSIP_MENU_OPTION",
            "CREATURE_TEMPLATE_VEHICLE",
            "SPELL",
            "SPELL_CLICK_EVENT",
            "QUEST_ACCEPT",
            "QUEST_SHOW_MARK",
            "VEHICLE_SPELL",
            "SMART_EVENT",
            "NPC_VENDOR",
            "SPELL_PROC",
            "TERRAIN_SWAP",
            "PHASE"});
            this.SourceTypeOrReferenceIDCMB.Location = new System.Drawing.Point(6, 38);
            this.SourceTypeOrReferenceIDCMB.Name = "SourceTypeOrReferenceIDCMB";
            this.SourceTypeOrReferenceIDCMB.Size = new System.Drawing.Size(255, 21);
            this.SourceTypeOrReferenceIDCMB.TabIndex = 0;
            this.SourceTypeOrReferenceIDCMB.SelectedIndexChanged += new System.EventHandler(this.SourceTypeOrReferenceIDCMB_SelectedIndexChanged);
            // 
            // SourceGroupTooltip
            // 
            this.SourceGroupTooltip.Popup += new System.Windows.Forms.PopupEventHandler(this.SourceGroupTooltip_Popup);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ElseGroupNUD);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(287, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(151, 79);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2.";
            // 
            // ElseGroupNUD
            // 
            this.ElseGroupNUD.Location = new System.Drawing.Point(10, 38);
            this.ElseGroupNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ElseGroupNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ElseGroupNUD.Name = "ElseGroupNUD";
            this.ElseGroupNUD.Size = new System.Drawing.Size(135, 20);
            this.ElseGroupNUD.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "ElseGroup";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ConditionValue3NUD);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.ConditionValue2NUD);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.ConditionValue1NUD);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Location = new System.Drawing.Point(288, 99);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(291, 213);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3.";
            // 
            // ConditionValue3NUD
            // 
            this.ConditionValue3NUD.Location = new System.Drawing.Point(101, 146);
            this.ConditionValue3NUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ConditionValue3NUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ConditionValue3NUD.Name = "ConditionValue3NUD";
            this.ConditionValue3NUD.Size = new System.Drawing.Size(92, 20);
            this.ConditionValue3NUD.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(103, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Condition Value 3";
            // 
            // ConditionValue2NUD
            // 
            this.ConditionValue2NUD.Location = new System.Drawing.Point(191, 88);
            this.ConditionValue2NUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ConditionValue2NUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ConditionValue2NUD.Name = "ConditionValue2NUD";
            this.ConditionValue2NUD.Size = new System.Drawing.Size(94, 20);
            this.ConditionValue2NUD.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(188, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Condition Value 2";
            // 
            // ConditionValue1NUD
            // 
            this.ConditionValue1NUD.Location = new System.Drawing.Point(7, 88);
            this.ConditionValue1NUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ConditionValue1NUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ConditionValue1NUD.Name = "ConditionValue1NUD";
            this.ConditionValue1NUD.Size = new System.Drawing.Size(90, 20);
            this.ConditionValue1NUD.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Condition Value 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "ConditionTypeOrReference";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "NONE",
            "AURA",
            "ITEM",
            "ITEM_EQUIPPED",
            "ZONE_ID",
            "REPUTATION_RANK",
            "TEAM",
            "SKILL",
            "QUEST_REWARDED",
            "QUEST_TAKEN",
            "DRUNKEN_STATE",
            "WORLD_STATE",
            "ACTIVE_EVENT",
            "INSTANCE_INFO",
            "QUEST_NONE",
            "CLASS",
            "RACE",
            "ACHIEVEMENT",
            "TITLE",
            "SPAWNMASK",
            "GENDER",
            "UNIT_STATE",
            "MAP_ID",
            "AREA_ID",
            "CREATURE_TYPE",
            "SPELL",
            "PHASEMASK",
            "LEVEL",
            "QUEST_COMPLETE",
            "NEAR_CREATURE",
            "NEAR_GAMEOBJECT",
            "OBJECT_ENTRY_GUID",
            "TYPE_MASK",
            "RELATION_TO",
            "REACTION_TO",
            "DISTANCE_TO",
            "ALIVE",
            "HP_VAL",
            "HP_PCT",
            "REALM_ACHIEVEMENT",
            "IN_WATER",
            "TERRAIN_SWAP",
            "STAND_STATE",
            "QUESTSTATE",
            "QUEST_OBJECTIVE_COMPLETE "});
            this.comboBox1.Location = new System.Drawing.Point(9, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(136, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 334);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Negative Condition";
            // 
            // NegativeConditionNUD
            // 
            this.NegativeConditionNUD.Location = new System.Drawing.Point(16, 351);
            this.NegativeConditionNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NegativeConditionNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.NegativeConditionNUD.Name = "NegativeConditionNUD";
            this.NegativeConditionNUD.Size = new System.Drawing.Size(94, 20);
            this.NegativeConditionNUD.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(145, 334);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Error Type";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(257, 334);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Error Text ID";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(391, 334);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "ScriptName";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 384);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Comment";
            // 
            // CommentTXT
            // 
            this.CommentTXT.Location = new System.Drawing.Point(16, 400);
            this.CommentTXT.Name = "CommentTXT";
            this.CommentTXT.Size = new System.Drawing.Size(369, 20);
            this.CommentTXT.TabIndex = 9;
            // 
            // ScriptNameTXT
            // 
            this.ScriptNameTXT.Location = new System.Drawing.Point(394, 350);
            this.ScriptNameTXT.Name = "ScriptNameTXT";
            this.ScriptNameTXT.Size = new System.Drawing.Size(153, 20);
            this.ScriptNameTXT.TabIndex = 10;
            // 
            // ErrorTypeNUD
            // 
            this.ErrorTypeNUD.Location = new System.Drawing.Point(148, 351);
            this.ErrorTypeNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ErrorTypeNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ErrorTypeNUD.Name = "ErrorTypeNUD";
            this.ErrorTypeNUD.Size = new System.Drawing.Size(88, 20);
            this.ErrorTypeNUD.TabIndex = 11;
            // 
            // ErrorTextIDNUD
            // 
            this.ErrorTextIDNUD.Location = new System.Drawing.Point(260, 351);
            this.ErrorTextIDNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ErrorTextIDNUD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ErrorTextIDNUD.Name = "ErrorTextIDNUD";
            this.ErrorTextIDNUD.Size = new System.Drawing.Size(90, 20);
            this.ErrorTextIDNUD.TabIndex = 12;
            // 
            // GenerateConditionBTN
            // 
            this.GenerateConditionBTN.Location = new System.Drawing.Point(417, 384);
            this.GenerateConditionBTN.Name = "GenerateConditionBTN";
            this.GenerateConditionBTN.Size = new System.Drawing.Size(75, 36);
            this.GenerateConditionBTN.TabIndex = 13;
            this.GenerateConditionBTN.Text = "Generate Condition";
            this.GenerateConditionBTN.UseVisualStyleBackColor = true;
            this.GenerateConditionBTN.Click += new System.EventHandler(this.GenerateConditionBTN_Click);
            // 
            // ResetBTN
            // 
            this.ResetBTN.Location = new System.Drawing.Point(498, 384);
            this.ResetBTN.Name = "ResetBTN";
            this.ResetBTN.Size = new System.Drawing.Size(75, 36);
            this.ResetBTN.TabIndex = 14;
            this.ResetBTN.Text = "Reset";
            this.ResetBTN.UseVisualStyleBackColor = true;
            this.ResetBTN.Click += new System.EventHandler(this.ResetBTN_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 435);
            this.Controls.Add(this.ResetBTN);
            this.Controls.Add(this.GenerateConditionBTN);
            this.Controls.Add(this.ErrorTextIDNUD);
            this.Controls.Add(this.ErrorTypeNUD);
            this.Controls.Add(this.ScriptNameTXT);
            this.Controls.Add(this.CommentTXT);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.NegativeConditionNUD);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "ConditionCreator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionTargetNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SourceIDNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SourceEntryNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SourceGroupNUD)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ElseGroupNUD)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionValue3NUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionValue2NUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConditionValue1NUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NegativeConditionNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorTypeNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorTextIDNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox SourceTypeOrReferenceIDCMB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown SourceEntryNUD;
        private System.Windows.Forms.NumericUpDown SourceGroupNUD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip SourceGroupTooltip;
        private System.Windows.Forms.ToolTip SourceEntryTooltip;
        private System.Windows.Forms.NumericUpDown SourceIDNUD;
        private System.Windows.Forms.Label SourceIDLBL;
        private System.Windows.Forms.ToolTip SourceIDTooltip;
        private System.Windows.Forms.ToolTip ConditionTargetTooltip;
        private System.Windows.Forms.NumericUpDown ConditionTargetNUD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox NotesRTB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown ElseGroupNUD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolTip ElseGroupTooltip;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.NumericUpDown ConditionValue3NUD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown ConditionValue2NUD;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ConditionValue1NUD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown NegativeConditionNUD;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox CommentTXT;
        private System.Windows.Forms.TextBox ScriptNameTXT;
        private System.Windows.Forms.NumericUpDown ErrorTypeNUD;
        private System.Windows.Forms.NumericUpDown ErrorTextIDNUD;
        private System.Windows.Forms.ToolTip ConditionValue1Tooltip;
        private System.Windows.Forms.ToolTip ConditionValue2Tooltip;
        private System.Windows.Forms.ToolTip ConditionValue3Tooltip;
        private System.Windows.Forms.ToolTip NegativeConditionTooltip;
        private System.Windows.Forms.ToolTip ErrorTypeTooltip;
        private System.Windows.Forms.ToolTip ErrorTextIdTooltip;
        private System.Windows.Forms.ToolTip ScriptNameTooltip;
        private System.Windows.Forms.Button GenerateConditionBTN;
        private System.Windows.Forms.Button ResetBTN;
    }
}

